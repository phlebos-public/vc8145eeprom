/*
 * This file is part of the vc8145eeprom program (https://gitlab.com/phlebos-public/vc8145eeprom.git).
 * Copyright (c) 2022 Lee Hornby.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>

class vc8145comm;

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

    const QString &serialportname() const;
    void setSerialportname(const QString &newSerialportname);

private slots:
    void updateSerportname(int idx);
    void dmmconnect();
    void dmmdisconnect();
    void eepromread();
    void displayeeprom(int errorcode);
    void saveBin();

private:
    void fillPortsInfo();

private:
    Ui::Widget *ui;
    vc8145comm *m_vc8145;
    QString m_serialportname;
    QByteArray m_eepromcontent;
};
#endif
