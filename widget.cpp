/*
 * This file is part of the vc8145eeprom program (https://gitlab.com/phlebos-public/vc8145eeprom.git).
 * Copyright (c) 2022 Lee Hornby.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "widget.h"
#include "./ui_widget.h"

#include "qdebug.h"
#include "vc8145comm.h"

#include <QSerialPortInfo>
#include <QMessageBox>
#include <QFileDialog>

static const char blankString[] = QT_TRANSLATE_NOOP("SettingsDialog", "N/A");

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget), m_vc8145(new vc8145comm)
{
    ui->setupUi(this);

    ui->disconnectButton->setEnabled(false);
    ui->readButton->setEnabled(false);
    ui->saveButton->setEnabled(false);

    connect(ui->connectButton, &QPushButton::clicked , this, &Widget::dmmconnect);
    connect(ui->disconnectButton, &QPushButton::clicked , this, &Widget::dmmdisconnect);
    connect(ui->readButton, &QPushButton::clicked , this, &Widget::eepromread);
    connect(ui->saveButton, &QPushButton::clicked , this, &Widget::saveBin);

    connect(ui->serialPortInfoListBox, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, &Widget::updateSerportname);
    connect(m_vc8145, &vc8145comm::eepromreadfinished, this, &Widget::displayeeprom);



    fillPortsInfo();
}

Widget::~Widget()
{
    delete ui;
}

void Widget::fillPortsInfo()
{
    ui->serialPortInfoListBox->clear();
    QString description;
    QString manufacturer;
    QString serialNumber;
    const auto infos = QSerialPortInfo::availablePorts();
    for (const QSerialPortInfo &info : infos) {
        QStringList list;
        description = info.description();
        manufacturer = info.manufacturer();
        serialNumber = info.serialNumber();
        list << info.portName()
             << (!description.isEmpty() ? description : blankString)
             << (!manufacturer.isEmpty() ? manufacturer : blankString)
             << (!serialNumber.isEmpty() ? serialNumber : blankString)
             << info.systemLocation()
             << (info.vendorIdentifier() ? QString::number(info.vendorIdentifier(), 16) : blankString)
             << (info.productIdentifier() ? QString::number(info.productIdentifier(), 16) : blankString);

        ui->serialPortInfoListBox->addItem(list.first(), list);
    }
}

const QString &Widget::serialportname() const
{
    return m_serialportname;
}

void Widget::setSerialportname(const QString &newSerialportname)
{
    m_serialportname = newSerialportname;
}

void Widget::updateSerportname(int idx)
{
    if (idx == -1)
        return;

    const QStringList list = ui->serialPortInfoListBox->itemData(idx).toStringList();
    setSerialportname(list.at(0));
}

void Widget::dmmconnect()
{

    QSerialPortInfo myport(serialportname());

    if (!myport.isNull())
    {

        ui->serialPortInfoListBox->setEnabled(false);
        ui->serportlabel->setEnabled(false);
        ui->connectButton->setEnabled(false);
        ui->disconnectButton->setEnabled(true);
        ui->readButton->setEnabled(true);

        m_vc8145->openSerialPort(serialportname());
    }
    else
    {
        QMessageBox::information(this, "Invalid port",serialportname());
    }
}

void Widget::dmmdisconnect()
{
    m_vc8145->closeSerialPort();
    ui->serialPortInfoListBox->setEnabled(true);
    ui->serportlabel->setEnabled(true);
    ui->connectButton->setEnabled(true);
    ui->disconnectButton->setEnabled(false);
    ui->readButton->setEnabled(false);

}

void Widget::eepromread()
{
    ui->plainTextEdit->clear();
    m_vc8145->readEEPROM();
    ui->readButton->setEnabled(false);
}

void Widget::displayeeprom(int errorcode)
{
    int eepromsize = 128;
    int slicesize = 16;


    char outsep(',');
    QByteArray eepromcontent;
    eepromcontent = m_vc8145->eepromcontent();

    m_eepromcontent = eepromcontent;

    qDebug() << "Widget::displayeeprom: " << errorcode  << eepromcontent.toHex(':');

    ui->plainTextEdit->clear();
    switch(errorcode) {
    case 0:
        for(int f = 0; f < eepromsize; f += slicesize)
        {
            QString tmpstring(eepromcontent.sliced(f, slicesize).toHex(outsep));
            tmpstring = "0x" + tmpstring;
            tmpstring.replace(QLatin1String(&outsep,1),(QLatin1String(&outsep,1) + QLatin1String(" 0x")));
            ui->plainTextEdit->appendPlainText(tmpstring);
        }
        ui->saveButton->setEnabled(true);
        break;
    case 1:
        ui->plainTextEdit->appendPlainText("Timeout Error");
        ui->saveButton->setEnabled(false);
        break;

    case 2: ;
        ui->plainTextEdit->appendPlainText("Sequence Error");
        ui->saveButton->setEnabled(false);
        break;
    }





    ui->readButton->setEnabled(true);
}

void Widget::saveBin()
{
    QString  fileName = QFileDialog::getSaveFileName(this,
                                                     tr("Save BIN"), QDir::homePath()+"/vc8145.BIN", tr("binary image files (*.bmp)"));
    QFile file(fileName);
    file.open(QIODevice::WriteOnly);
    file.write(m_eepromcontent);
    file.close();
}


