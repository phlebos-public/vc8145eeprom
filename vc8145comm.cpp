/*
 * This file is part of the vc8145eeprom program (https://gitlab.com/phlebos-public/vc8145eeprom.git).
 * Copyright (c) 2022 Lee Hornby.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include "vc8145comm.h"
#include "qcoreevent.h"

#include <QSerialPort>
#include <QMap>

#include <QDebug>

#define RESPONSETIMEOUT 500

const QMap<Cmd, CmdInfo> vc8145comm::cmdInfos = {
    { Cmd::readEEPROM,     { "EEProm Read",     9  } },
};

vc8145comm::vc8145comm(QObject *parent): QObject{parent}, m_serial(new QSerialPort(this))
            , m_responsetimerid(0)
{
    connect(m_serial, &QSerialPort::readyRead, this, &vc8145comm::readPort);
}



void vc8145comm::sendcmd(QByteArray cmd)
{

    m_serial->write(cmd);
    m_responsetimerid = startTimer(RESPONSETIMEOUT);

    m_lastcommand = cmd;

        qDebug() << "vc8145comm::sendcmd: " << cmd.toHex(':');
}



void vc8145comm::readPort()
{
    if(m_responsetimerid != 0)
    {
        killTimer(m_responsetimerid);
        m_responsetimerid = 0;
    }
    while (m_serial->canReadLine()) {
        const QByteArray packet = m_serial->readLine();

        qDebug() << "vc8145comm::readPort: " << packet.toHex(':');
        if(packet != m_lastcommand)
        {
            int f = 0;
            qDebug() << "vc8145comm::readPort: Loop: " << f++;
            quint8 cmdval = packet[0];
            CmdInfo cmdinfo = cmdInfos.value((Cmd)cmdval,{"Unknown",0});

            if((packet.size() == cmdinfo.outputLen) && checkpacket(packet) && packet[0] == m_lastcommand[0])
            {
                 qDebug() << "vc8145comm::haveGoodPacket: " << packet.toHex(':');

                emit haveGoodPacket(packet);

            }
            else
            {

                qDebug() << "vc8145comm::BadPacket: " << packet.toHex(':');

                sendcmd(m_lastcommand);
            }
        }
    }


}



bool vc8145comm::openSerialPort(QString portname)
{
    m_serial->setPortName(portname);
    m_serial->setBaudRate(QSerialPort::Baud9600);
    m_serial->setDataBits(QSerialPort::Data8);
    m_serial->setParity(QSerialPort::NoParity);
    m_serial->setStopBits(QSerialPort::OneStop);
    m_serial->setFlowControl(QSerialPort::NoFlowControl);

    if (m_serial->open(QIODevice::ReadWrite)) {
        return true;
    }
    return false;
}

void vc8145comm::closeSerialPort()
{
    if (m_serial->isOpen())
        m_serial->close();
}

quint8 vc8145comm::calc_checksum(const QByteArray indata)
{
    quint8 checksum, tmpbyte;
    quint8 datasize;
    datasize = indata.size();

    checksum = 0;

    for(int f = 0; f < datasize - 2; f++)
    {
        checksum ^= indata[f];
    }


    tmpbyte = checksum;
    tmpbyte = (tmpbyte >> 2);
    tmpbyte &= (1 << 4);
    checksum ^= tmpbyte;

    tmpbyte = checksum;
    tmpbyte = (tmpbyte >> 2);
    tmpbyte &= (1 << 5);
    checksum ^= tmpbyte;

    checksum &= 0x3f;

    checksum += 0x22;


    return checksum;
}

bool vc8145comm::checkpacket(QByteArray inpacket)
{
    bool retval = false;
    quint8 packetsize, checksum ,checksumbyte;

    packetsize = inpacket.size();
    if(packetsize > 1)
    {
        checksumbyte = inpacket[packetsize - 2];
        checksum = calc_checksum(inpacket);
        if(checksum == checksumbyte) retval = true;
        qDebug() << "vc8145comm::checkpacket: " << inpacket.toHex(':') << QString::number(checksumbyte, 16) << QString::number(checksum, 16);;
    }
    else retval = true;

    return retval;
}

QByteArray vc8145comm::eepromReadwordcmd(quint8 address)
{
    quint8 lowbyte,highbyte;
    lowbyte = (0x0f & address)^0x40;
    highbyte = ((0xf0 & address) >> 4)^0x40;
    QByteArray outcmd(1, (quint8)Cmd::readEEPROM);
    outcmd.append(highbyte);
    outcmd.append(lowbyte);
    outcmd.append(0x0a);

    return outcmd;
}

eeprompacket vc8145comm::eepromPacketdecode(const QByteArray indata)
{
    eeprompacket retval;

    retval.address = (indata[1] << 4) | (indata[2] & 0x0f);

    retval.highbyte = (indata[3] << 4) | (indata[4] & 0x0f);
    retval.lowbyte =  (indata[5] << 4) | (indata[6] & 0x0f);

    return retval;

}

void vc8145comm::readEEPROM()
{
    m_eepromcontent.clear();
    m_startword = 0;
    m_finishword = 63;

    m_count = m_startword;
    qDebug() << "***** vc8145comm::readEEPROM: " <<  m_startword <<  m_finishword << m_count;

    connect(this, &vc8145comm::haveGoodPacket, this, &vc8145comm::eepromwordReceiver);
    connect(this, &vc8145comm::responsetimeout, this, &vc8145comm::eepromendread);
    eepromwordRequester();
}

void vc8145comm::eepromwordRequester()
{
    QByteArray readcmd;
    readcmd = eepromReadwordcmd(m_count);
    sendcmd(readcmd);
}

const QByteArray &vc8145comm::eepromcontent() const
{
    qDebug() << "vc8145comm::eepromcontent: " << m_eepromcontent.toHex(':');
    return m_eepromcontent;
}

void vc8145comm::eepromendread(int errorcode)
{
    disconnect(this, &vc8145comm::responsetimeout, this, &vc8145comm::eepromendread);
    disconnect(this, &vc8145comm::haveGoodPacket, this, &vc8145comm::eepromwordReceiver);
    emit eepromreadfinished(errorcode);
}

void vc8145comm::eepromwordReceiver(QByteArray inpacket)
{
    eeprompacket inbound;

    inbound = eepromPacketdecode(inpacket);
    qDebug() << "vc8145comm::eepromwordReceiver: " << inpacket.toHex(':') << inbound.address;
    if(inbound.address == m_count)
    {



        m_eepromcontent.append(inbound.lowbyte);
        m_eepromcontent.append(inbound.highbyte);
        qDebug() << "vc8145comm::eepromwordReceiver: Correct :" << inpacket.toHex(':') << m_eepromcontent.toHex(':')
                 << inbound.address << QString::number(inbound.highbyte, 16)<< QString::number(inbound.lowbyte, 16);
        if(m_count < m_finishword)
        {
            m_count++;
            eepromwordRequester();
        }
        else eepromendread(0);


    }
    else
    {


        qDebug() << "vc8145comm::eepromwordReceiver: expecting: " << m_count << " got " <<  inbound.address;
        eepromendread(2);
    }
}

void vc8145comm::timerEvent(QTimerEvent *event)
{
    if(event->timerId() == m_responsetimerid)
    {
        qDebug() << "vc8145comm::timerEvent: ";
        emit responsetimeout(1);
        killTimer(m_responsetimerid);
    }
}
