/*
 * This file is part of the vc8145eeprom program (https://gitlab.com/phlebos-public/vc8145eeprom.git).
 * Copyright (c) 2022 Lee Hornby.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VC8145COMM_H
#define VC8145COMM_H

#include <QObject>
#include <QByteArray>
#include <QDataStream>

class QSerialPort;

enum class Cmd : quint8 {
    readEEPROM     = 0xD0,
};

struct CmdInfo {
    QString name;
    size_t outputLen;
};

struct eeprompacket {
    quint8 address;
    quint8 highbyte;
    quint8 lowbyte;
};

class vc8145comm : public QObject
{
    Q_OBJECT
public:
    explicit vc8145comm(QObject *parent = nullptr);


    bool openSerialPort(QString portname);
    void closeSerialPort();

    void sendcmd(QByteArray cmd);
    void readEEPROM();

    const QByteArray &eepromcontent() const;

signals:
    void haveGoodPacket(QByteArray);
    void eepromreaderror();
    void eepromreadfinished(int errorcode);
    void responsetimeout(int errorcode);

private slots:
    void readPort();
    void eepromwordReceiver(QByteArray inpacket);
    void eepromendread(int errorcode);

protected:
    void timerEvent(QTimerEvent *event) override;

private:



    quint8 calc_checksum(const QByteArray indata);
    bool checkpacket(QByteArray inpacket);
    QByteArray eepromReadwordcmd(quint8 address);
    eeprompacket eepromPacketdecode(const QByteArray indata);
    void eepromwordRequester();



    QSerialPort *m_serial = nullptr;
    QByteArray m_lastcommand;
    QByteArray m_eepromcontent;
    quint8 m_startword;
    quint8 m_finishword;
    quint8 m_count;
    int m_responsetimerid;

    static const QMap<Cmd, CmdInfo> cmdInfos;
};

#endif
